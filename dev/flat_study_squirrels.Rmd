---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)
library(dplyr)
library(ggplot2)
library(tools)
library(purrr)
library(readxl)

# nyc_squirrels_17 <- readxl::read_excel(
#   path = system.file("nyc_squirrels_17.xlsx", 
#                      package = "squirrelsjade"))
# nyc_squirrels_18 <- readxl::read_excel(
#   path = system.file("nyc_squirrels_18.xlsx", 
#                      package = "squirrelsjade"))
# 
# possi_excel <- possibly(read_excel, otherwise = "NULL")
# 
# data_nyc_squirrels_17oct <- possi_excel(path = system.file("nyc_squirrels_17.xlsx", package = "squirrelsjade"))
# data_nyc_squirrels_17oct
# 
# data_nyc_squirrels_18oct <- possi_excel(
#   path = system.file("nyc_squirrels_18.xlsx", 
#                      package = "squirrelsjade"))
# data_nyc_squirrels_18oct
# 
# files_nyc_squirrels <- list.files(
#   path = system.file(package = "squirrelsjade"),
#   pattern = "nyc_squirrels_[0-9][0-9].xlsx", 
#   full.names = TRUE)
# 
# data_nyc_squirrels_oct <- 
#   map(
#   .x = files_nyc_squirrels,
#   .f = possi_excel
#   ) 

```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)

```

# Get a message with a fur color

You can get a message with the fur color of interest with `get_message_fur_color()`.

```{r function-get_message_fur_color}
#' Get a message with the fur color of interest.
#' 
#' @param primary_fur_color Character. The primary fur color of interest
#' 
#' @importFrom glue glue
#' 
#' @return Used for side effect. Outputs a message in the console
#' 
#' @export
get_message_fur_color <- function(primary_fur_color) {
  message(glue("We will focus on {primary_fur_color} squirrels"))
}
```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Black")
get_message_fur_color(primary_fur_color = "Cinnamon")
```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Black"), 
    regexp = "We will focus on Black squirrels"
  )
})
```

# Study activity
    
```{r function-study_activity}
#' Etudier les activités des écureuils
#' 
#' @param df_squirrels_act Un dataframe avec les activités des écureuils et une colonne contenant la couleur de leurs fourrures
#' @param col_primary_fur_color Une chaîne de caractères avec la couleur des écureuils. 
#' 
#' @return Un tableau et un graphe avec les activités des écureuils en fonction de la couleur de la fourrure
#' 
#' @importFrom dplyr filter
#' @importFrom ggplot2 ggplot aes geom_col labs scale_fill_manual
#' @importFrom glue glue
#' 
#' @export
study_activity <- function(df_squirrels_act, col_primary_fur_color) {

  if (!is.data.frame(df_squirrels_act)) {
    stop("`df_squirrels_act` doit \u00eatre un data.frame")
  }
  if (!is.character(col_primary_fur_color)) {
    stop("`col_primary_fur_color` doit \u00eatre un character")
  }

  check_squirrel_data_integrity(df_squirrels = df_squirrels_act)
  
  table <- df_squirrels_act %>%
    filter(primary_fur_color == col_primary_fur_color)

  graph <- table %>%
    ggplot() +
    aes(x = activity, y = counts, fill = age) +
    geom_col() +
    labs(x = "Type of activity",
         y = "Number of observations",
         title = glue("Type of activity by age for {tolower(col_primary_fur_color)} squirrels")) +
    scale_fill_manual(name = "Age",
                      values = c("#00688B", "#00BFFF"))

  return(list(table = table, graph = graph))
}

```
  
```{r example-study_activity}
data(data_act_squirrels)
study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")
```
  
```{r tests-study_activity}
test_that("study_activity works", {
   output <- study_activity(
    df_squirrels_act = data_act_squirrels, 
    col_primary_fur_color = "Gray"
  )
  
  expect_s3_class(
    output[["table"]],
    "data.frame"
  )
  expect_s3_class(
    output[["graph"]],
    c("gg", "ggplot")
  )
})

test_that("study_activity errors", {
   expect_error(
    object = study_activity(df_squirrels_act = 1,
                            col_primary_fur_color = "Gray"),
    regexp = "`df_squirrels_act` doit \u00eatre un data.frame" 
  )
  expect_error(
    object = study_activity(df_squirrels_act = data_act_squirrels,
                            col_primary_fur_color = 1),
    regexp = "`col_primary_fur_color` doit \u00eatre un character"                                       
  )
})

```
    
# Save as csv
    
```{r function-save_as_csv}
#' Enregistrer le jeu de données en csv
#' 
#' @param df Le jeu de données à enregistrer
#' @param chemin Une chaîne de caractères qui correspond au chemin du fichier
#' @param ... Des paramètres supplémentaires
#' 
#' @return Le chemin complet du csv créé
#' 
#' @importFrom utils write.csv2
#' @importFrom tools file_ext 
#' 
#' @export
save_as_csv <- function(df, chemin, ...){
  if (!is.data.frame(df)) {
    stop("`df` doit \u00eatre un data.frame")
  }
  if (!is.character(chemin)) {
    stop("`chemin` doit \u00eatre un character")
  }
 
  if (isFALSE(file_ext(chemin) == "csv")){
    stop("L\'extension du fichier \u00e0 cr\u00e9er n\'est pas la bonne. Elle doit \u00eatre en `csv`")
  }
  
  if (isFALSE(dir.exists(dirname(chemin)))) {
    stop("Le chemin o\u00f9 doit \u00eatre enregistr\u00e9 le fichier n\'existe pas.")
  }
  
  write.csv2(
    x = df, 
    file = chemin,
    row.names = FALSE,
    ...
  )
  
  return(invisible(chemin))
}
```
  
```{r example-save_as_csv}
# Create a temporary directory
my_temp_folder <- tempfile(pattern = "savecsv")
dir.create(my_temp_folder)

# Perform the analysis with study_activity()
res_activities_grat_squirrels <- study_activity(
  df_squirrels_act = data_act_squirrels, 
  col_primary_fur_color = "Gray"
  )

# Save res_activities_grat_squirrels$table as a csv
save_as_csv(res_activities_grat_squirrels$table, 
            chemin = file.path(my_temp_folder, "my_filtered_activities.csv"))

# See if the csv exists
list.files(my_temp_folder)

# Read the csv
read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))

# Delete the temporary folder
unlink(my_temp_folder, recursive = TRUE)
```
  
```{r tests-save_as_csv}
test_that("save_as_csv works", {
  
  test_dir <- tempfile(pattern = "test_dir")
  dir.create(test_dir)
  test_path <- file.path(
    test_dir,
    "output.csv"
  )
  
  output_path <- save_as_csv(
    df = iris,
    chemin = test_path
  )
  
  expect_true(
    file.exists(test_path)
  )
  
  expect_equal(
    object = output_path,
    expected = test_path
  )
  
  iris_output <- read.csv2(
    file = test_path,
    stringsAsFactors = TRUE
  )
  expect_equal(
    object = iris_output,
    expected = iris
  )
  
  unlink(
    test_dir,
    recursive = TRUE, 
    force = TRUE
  )
})


test_that("save_as_csv retourne les erreurs attendues", {
  expect_error(
    object =  iris %>% save_as_csv("output.xlsx"),
    regexp = "L\'extension du fichier \u00e0 cr\u00e9er n\'est pas la bonne. Elle doit \u00eatre en `csv`"
  )
  expect_error(
    object = NULL %>% save_as_csv("output.csv"),
    regexp = "`df` doit \u00eatre un data.frame"
  )
  expect_error(
    object = iris %>% save_as_csv("nexiste/pas/output.csv"),
    regexp = "Le chemin o\u00f9 doit \u00eatre enregistr\u00e9 le fichier n\'existe pas."
  )
})
```

# Read data day squirrels
    
```{r function-read_data_day_squirrels}
#' Importer tous les fichiers avec les données sur les écureuils
#' 
#' @param path Chemin du dossier avec les fichier sur les écureuils
#' 
#' @return Une liste de données sur les écureuils
#' 
#' @importFrom purrr map possibly 
#' @importFrom readxl read_excel
#' 
#' @export
read_data_day_squirrels <- function(path) {
  
  if (isFALSE(dir.exists(dirname(path)))) {
    stop("Le chemin n\'existe pas.")
  }
  
  possi_excel <- possibly(read_excel, otherwise = "NULL")
  
  files <- list.files(path = path,
                      pattern = "nyc_squirrels_[0-9][0-9].xlsx",
                      full.names = TRUE)
  
  if (length(files) == 0) {
    stop("Le dossier ne contient pas les donn\u00e9es sur les \u00e9cureuils.")
  }
  
  data_nyc_squirrels <-
    map(.x = files,
        .f = possi_excel)
  
  return(data_nyc_squirrels)
  
}
```
  
```{r example-read_data_day_squirrels}
data_squirrels_oct <- read_data_day_squirrels(
  path = system.file(package = "squirrelsjade"))

# Affichage du nombre de lignes pour chaque table
data_squirrels_oct %>%
  map(dim)
# Affichage des imports qui ont fonctionné uniquement
data_squirrels_oct %>%
  purrr::compact()
```
  
```{r tests-read_data_day_squirrels}
test_that("read_data_day_squirrels works", {
  expect_true(inherits(read_data_day_squirrels, "function")) 
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", 
               vignette_name = "Study the squirrels",
               overwrite = TRUE)
```
